# Desktop Dungeons Simulator

`dd_sim` is a project devoted to implementing combat logic used in Desktop Dungeons and simulating that in Rust. Currently it supports very basic swing-fights (clickfighting), but hopefully will support spell glyphs eventually to simulate what real players might do in Desktop Dungeons to get a win.

The battle simulation involves two entities: a `Player` and a `Monster`, all of which use an underlying `EntityData` struct. To make a new Human Thief, you'd initiate it with the following line:

```rust
let player = Entity::new_player(1, Race::Human, Class::Thief);
```

Using enumerations has the advantage of making matches more easier as opposed to checking against a struct with more labels.

More soon(tm)
