// data/class.rs

/// The Class enum holds all of the classes in Desktop Dungeons.
/// Each class has specific traits that determine the result of combat,
/// usually with damage bonuses or specific edge-cases like Assassin
/// insta-kills.
pub enum Class {
    Fighter, Berserker, Warlord,
    Thief,   Rogue,     Assassin,
    Wizard,  Sorcerer,  Bloodmage,
    Priest,  Monk,      Paladin,
}


impl Class {

    /// Turn a Class value into a String representaton.
    ///
    /// ```
    /// let class = Class::Fighter;
    /// let name = class.to_string(); // becomes "Fighter"
    /// ```
    pub fn to_string(&self) -> String {
        match *self {
            Class::Fighter   => "Fighter".into(),
            Class::Berserker => "Berserker".into(),
            Class::Warlord   => "Warlord".into(),
            Class::Thief     => "Thief".into(),
            Class::Rogue     => "Rogue".into(),
            Class::Assassin  => "Assassin".into(),
            Class::Wizard    => "Wizard".into(),
            Class::Sorcerer  => "Sorcerer".into(),
            Class::Bloodmage => "Bloodmage".into(),
            Class::Priest    => "Priest".into(),
            Class::Monk      => "Monk".into(),
            Class::Paladin   => "Paladin".into(),
        }
    }
}


// end data/class.rs
