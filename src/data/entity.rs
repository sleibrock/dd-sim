// data/entity.rs

use data::race::{Race, Monster};
use data::class::Class;


/// The Attack enum is a type expressing the different range of results
/// from a single "swing" attack in battle. These range from doing
/// a certain amount of damage, to kills, to other such effects like
/// mana burn or death protection being removed from the target.
pub enum Attack {
    Kill,           // opponent was killed normally
    Instakill,      // opponent was instantly killed (assassin)
    Petrified,      // opponent was petrified (Gorgons)
    DeathProt,      // opponent survived with death protection
    Damaged(i16),   // opponent was damaged by some value
    ManaBurn(i16),  // opponent was mana-burned and damaged
    Poisoned(i16),  // opponent was poisoned (Serpents)
}


/// Entity is the algebraic type expressing either a Player or a Monster.
/// A Monster takes an EntityData struct and a Monster race type.
/// A Player takes an EntityData, a Player Race, and a Player Class type.
///
/// ```
/// let player = Entity::new_player(1, Race::Human, Class::Fighter)?;
/// let monster = Entity::new_monster(1, Monster::Meatman)?;
/// ```
pub enum Entity {
    Monster(EntityData, Monster),
    Player(EntityData, Race, Class),
}


/// EntityData is the internal data for game objects (Player and Monster)
/// which keeps track of values like health points and damage. Other values
/// are also stored for attack calculations like resistance or bonus damage.
/// Certain values are also stored to determine bonus effects like a
/// Priest vs. an Undead monster
pub struct EntityData {
    pub level:         i16,
    pub hp:            i16,
    pub max_hp:        i16,
    pub mp:            i16,
    pub bonus_damage:  f32,  // 1.0 = 0% attack bonus damage 
    pub damage:        i16,
    pub first_strike:  bool,
    pub mag_res:       f32,  // 1.0 = 0% resistance,
    pub phys_res:      f32,  // 1.0 = 0% resistance
    pub life_steal:    f32,
    pub corrosion:     i16,
    pub corro_stacks:  i16,
    pub death_gaze:    f32,  // 0.0 = inactive, 0.5 = 50% dg, 1.0 = 100% dg 
    pub death_prot:    i16,
    pub is_magic:      bool, // determines if it's magical damage
    pub is_assassin:   bool, // one-shots lower level enemies
    pub is_undead:     bool, // used to calculate undead damage (priest)
    pub is_thief:      bool, // bonus damage vs full hp opponents
    pub is_priest:     bool, // bonus damage vs undead
}



/// EntityData covers both internal data for Players and Monsters
impl EntityData {

    /// Return an EntityData struct with all fields set to default values.
    pub fn blank() -> EntityData {
        EntityData {
            level:        1,
            hp:           10,
            max_hp:       10,
            mp:           10,
            bonus_damage: 1.0,     // 1.0 = 0% attack bonus, 0.7 = -30%
            damage:       5,
            first_strike: false, 
            mag_res:      1.0,     // resists start at 1.0 (100% damage taken) 
            phys_res:     1.0,
            life_steal:   0.0,
            death_gaze:   0.0,
            corrosion:    0,       // how much corrosion is afflicted in each atk
            corro_stacks: 0,       // how much corrosion one currently has
            death_prot:   0,       // keeps track of num of death prot layers
            is_magic:     false,   // determines if your attack is magic or not
            is_assassin:  false,   // kills opponent if they are lower level 
            is_thief:     false,   // damage bonus when opponent is 100% hp
            is_priest:    false,   // 100% damage bonus vs undead
            is_undead:    false,   // see is_priest comment
        }
    }
    
    /// Initialize a new player data struct. This should only be used
    /// by the Entity functions to generate new Player entities
    pub fn player(level: i16, class: &Class) -> Result<EntityData, String> {
        if level <= 0 {
            return Err("Given level is not allowed (level > 0)".into());
        }

        let mut data = EntityData::blank();
        data.level  = level;
        data.hp     = level * 10;
        data.max_hp = data.hp;
        data.damage = level * 5;

        // match against all classes and mutate the data template 
        match class {
            Class::Fighter => {
                data.death_prot   = 1;
            }
            Class::Rogue => {
                data.first_strike = true;
                data.bonus_damage = 1.4;
                data.hp           = 5 * level;
                data.max_hp       = data.hp;
            }
            Class::Thief => {
                data.is_thief     = true;
            }
            Class::Assassin => {
                data.is_assassin  = true;
            }
            Class::Berserker => {
                data.bonus_damage = 1.2;
                data.mag_res      = 0.5;
            }
            Class::Monk => {
                data.damage       = 3 * level;
                data.bonus_damage = 0.7;
                data.phys_res     = 0.25;
            }
            Class::Priest => {
                data.is_priest    = true;
            }
            _ => {}
        }
        return Ok(data);
    }


    /// Initialize a new Monster data struct. This should only be used
    /// by the Entity function to generate a new data struct
    pub fn monster(level: i16, race: &Monster) -> Result<EntityData, String> {
        if level <= 0 {
            return Err("Given level is not allowed (level > 0)".into());
        }

        let mut data = EntityData::blank();

        let base_hp  : i16 = (level*(level+6)) - 1; //(n(n+6)-1)
        let base_dmg : i16 = (level*(level+5)) / 2; //(n*(n+5))/2

        // unpack the monster's growth value pair
        let (dmg_growth, hp_growth) = race.growth();

        match race {
            Monster::Goblin => {
                data.first_strike = true;
            }
            Monster::Gorgon => {
                data.death_gaze   = 0.5; // 50% death gaze
            }
            Monster::Goat => {
                data.mag_res      = 0.75    // 25% magic res
            }
            Monster::Zombie => {
                data.is_undead    = true;
            }
            Monster::Wraith => {
                data.is_undead    = true;
            }
            _ => {}
        }
        data.level  = level;
        data.hp     = ((base_hp as f32) * hp_growth) as i16;
        data.max_hp = data.hp;
        data.mp     = 0;
        data.damage = ((base_dmg as f32) * dmg_growth) as i16;
        return Ok(data);
    }

    /// Apply a damage swing from one data struct to another.
    /// This function will calculate the damage, the opponent's resistances,
    /// and other side-effects like insta-kills, class perks, monster abilities,
    /// and other such things.
    /// The "other" struct must be mutable in order to mutate it's HP value.
    pub fn damage_swing(&self, other: &mut EntityData) -> Attack {
        // assassin insta-kills lower level monsters
        if self.is_assassin && self.level > other.level {
            other.hp = 0;
            return Attack::Instakill;
        }

        // do bonus damage against full hp opponents
        let mut extra : f32 = 0.0;
        if self.is_thief && other.hp == other.max_hp {
            extra = 0.3;
        }

        // calculate extra damage for priest (100%, or 1.0)
        if self.is_priest && other.is_undead {
            extra = 1.0;
        }
        
        let d = self.damage as f32;
        let outgoing_damage : i16 = match self.is_magic {
            true =>  ((d * (self.bonus_damage + extra)) * other.mag_res) as i16, 
            false => ((d * (self.bonus_damage + extra)) * other.phys_res) as i16, 
        };
        
        other.hp -= outgoing_damage;

        // check if opponent is below the death gaze calculation
        // if opponent has death prot, set to 1 hp instead
        if self.death_gaze > 0.0 {
            let threshold = ((other.max_hp as f32) * self.death_gaze) as i16;
            if other.hp < threshold {
                if other.death_prot > 0 {
                    other.hp = 1;
                    other.death_prot -= 1;
                    return Attack::DeathProt;
                }
                other.hp = 0;
                return Attack::Petrified;
            }
        }
        
        if other.hp <= 0 {
            if other.death_prot > 0 {
                other.hp = 1;
                other.death_prot -= 1;
                return Attack::DeathProt;
            }
            other.hp = 0;
            return Attack::Kill;
        }
        return Attack::Damaged(outgoing_damage);
    }
}

/// The Entity enumeration abstracts over the two types (Player and Monster).
impl Entity {

    /// Convert an Entity to a (short) String representation.
    /// ```
    /// let player = Entity::new_player(1, Race::Human, Class::Fighter)?;
    /// let name = player.to_string(); // becomes "Human Fighter"
    /// ```
    pub fn to_string(&self) -> String {
        match *self {
            Entity::Monster(_, ref r) => r.to_string(),
            Entity::Player(_, ref pr, ref pc) => format!(
                "{} {}", pr.to_string(), pc.to_string(),
            ),
        }
    }

    /// Convert an Entity to a String representation.
    /// ```
    /// let player = Entity::new_player(1, Race::Human, Class::Fighter)?;
    /// let name = player.full_name(); // becomes "Level 1 Human Fighter"
    /// ```
    pub fn full_name(&self) -> String {
        match *self {
            Entity::Monster(ref d, ref r) => format!(
                "Level {} {}",
                d.level,
                r.to_string(),
            ),
            
            Entity::Player(ref d, ref pr, ref pc) => format!(
                "Level {} {} {}",
                d.level,
                pr.to_string(),
                pc.to_string(),
            ),
        }
    }

    /// Returns a reference to the Entity's internal data struct.
    pub fn data(&self) -> &EntityData {
        match *self {
            Entity::Monster(ref d, _) => d,
            Entity::Player(ref d, _, _) => d,
        }
    }

    /// Returns a mutable reference to the Entity's internal data struct.
    /// Used mostly for damage swing calculations to mutate HP values.
    pub fn m_data(&mut self) -> &mut EntityData {
        match *self {
            Entity::Monster(ref mut d, _) => d,
            Entity::Player(ref mut d, _, _) => d,
        }
    }

    /// Return the Entity's level.
    pub fn level(&self) -> i16 {
        self.data().level
    }

    /// Return the Entity's HP value.
    pub fn hp(&self) -> i16 {
        self.data().hp
    }

    /// Return the Entity's MP value.
    pub fn mana(&self) -> i16 {
        self.data().mp
    }

    /// Return the Entity's damage value.
    pub fn damage(&self) -> i16 {
        self.data().damage
    }

    /// Return whether the Entity has first strike.
    pub fn first_strike(&self) -> bool {
        self.data().first_strike
    }

    /// Print out a small status string to stdout.
    /// Used mostly in battle turns.
    pub fn status(&self) {
        match *self {
            Entity::Monster(_, _) => println!(
                "{} has {} health left",
                self.to_string(), self.hp(),
            ),
            Entity::Player(_, _, _) => println!(
                "{} has {} health and {} mana",
                self.to_string(), self.hp(), self.mana(),
            ),
        }
    }


    /// Generate a new player when given a level, a race and a class.
    /// Yields an Error if there was an issue creating the Entity.
    /// ```
    /// let player = Entity::new_player(1, Race::Human, Class::Fighter)?;
    /// ```
    pub fn new_player(lvl: i16, r: Race, c: Class) -> Result<Entity, String> {
        Ok(Entity::Player(EntityData::player(lvl, &c)?, r, c))
    }


    /// Generate a new player when given a level, a race and a class.
    /// Yields an Error if there was an issue creating the Entity.
    ///
    /// ```
    /// let monster = Entity::new_monster(1, Monster::Meatman)?;
    /// ```
    pub fn new_monster(lvl: i16, r: Monster) -> Result<Entity, String> {
        Ok(Entity::Monster(EntityData::monster(lvl, &r)?, r))
    }

    
    /// Make one Entity swing at another Entity. The target Entity
    /// must be mutable (in order to lower their HP).
    ///
    /// ```
    /// let mut monster = Entity::new_monster(1, Monster::Meatman)?;
    /// let mut player = Entity::new_player(1, Race::Human, Class::Fighter)?;
    /// player.swing_at(&mut monster);
    /// ```
    pub fn swing_at(&mut self, other: &mut Entity) {
        let d = &self.m_data().damage_swing(other.m_data());

        match d {
            Attack::Instakill => println!("{} kills {} instantly!",
                                     &self.to_string(),
                                     other.to_string(),
            ),
            Attack::Kill => println!("{} kills {}!",
                                     &self.to_string(),
                                     other.to_string(),
            ),
            Attack::Damaged(x) => println!("{} swings at {} for {} damage",
                                          &self.to_string(),
                                          other.to_string(),
                                          x,
            ),
            Attack::Petrified => println!("{} petrifies {}!",
                                        &self.to_string(),
                                        other.to_string(),
            ),
            Attack::DeathProt => println!("{} barely survives {}'s attack!",
                                             other.to_string(),
                                             &self.to_string(),
            ),
            _ => {},
        }
    }
}


// end data/entity.rs

