// data/race.rs


/// The Race enumeration is used for non-monster Entity types,
/// or Player-type characters. These races are specific to CP
/// conversion bonuses and not actual combat-related data.
///
/// ```
/// let human = Race::Human;
/// ```
pub enum Race {
    Human, Elf, Dwarf, Halfling, Gnome, Orc, Goblin, Goat,
}


/// The Monster enumeration holds all the races used to create Monster
/// entities. Each value has specific values that need to be allocated
/// by the Entity constructor depending on which race gets passed in.
/// Each monster has specific growth values associated with their Race.
pub enum Monster {
    Bandit,  Dragon,  Goat,   Goblin,
    Golem,   Blob,    Gorgon, Meatman,
    Serpent, Warlock, Wraith, Zombie,
}


impl Race {
    /// Convert a Race type into a String representation.
    ///
    /// ```
    /// let human = Race::Human;
    /// let name = human.to_string(); // "Human"
    /// ```
    pub fn to_string(&self) -> String {
        match *self {
            Race::Human    => "Human".into(),
            Race::Elf      => "Elf".into(),
            Race::Dwarf    => "Dwarf".into(),
            Race::Halfling => "Halfling".into(),
            Race::Gnome    => "Gnome".into(),
            Race::Orc      => "Orc".into(),
            Race::Goblin   => "Goblin".into(),
            Race::Goat     => "Goat".into(),
        }
    }
}


impl Monster {
    /// Convert a Race type into a String representation.
    ///
    /// ```
    /// let monster = Monster::Bandit;
    /// let name = monster.to_string(); // "Bandit"
    /// ```
    pub fn to_string(&self) -> String {
        match *self {
            Monster::Bandit  => "Bandit".into(),
            Monster::Dragon  => "Dragon Spawn".into(),
            Monster::Goat    => "Goat".into(),
            Monster::Goblin  => "Goblin".into(),
            Monster::Golem   => "Golem".into(),
            Monster::Blob    => "Goo Blob".into(),
            Monster::Gorgon  => "Gorgon".into(),
            Monster::Meatman => "Meat Man".into(),
            Monster::Serpent => "Serpent".into(),
            Monster::Warlock => "Warlock".into(),
            Monster::Wraith  => "Wraith".into(),
            Monster::Zombie  => "Zombie".into(),
        }
    }

    /// Returns a pair representing the associated monster's
    /// growth values, specifically in the format of (damage, hp).
    /// These values determine the rate at which the monster's
    /// HP and damage values will grow per-level (1.0 means 100%,
    /// 0.5 means 50% growth per level (5 hp instead of 10)).
    ///
    /// ```
    /// let monster = Monster::Bandit;
    /// let (dam, hp) = monster.growth();
    /// let true_damage = 50 * dam; // use the damage growth value
    /// ```
    pub fn growth(&self) -> (f32, f32) {
        match *self {
            Monster::Bandit  => (1.0,   1.0), 
            Monster::Dragon  => (1.25,  1.0),
            Monster::Goat    => (1.0,   0.9),
            Monster::Goblin  => (1.2,   1.0),
            Monster::Golem   => (1.0,   1.0),
            Monster::Blob    => (1.0,   1.0),
            Monster::Gorgon  => (1.0,   0.9),
            Monster::Meatman => (0.65,  2.0),
            Monster::Serpent => (1.0,   1.0),
            Monster::Warlock => (1.35,  1.0),
            Monster::Wraith  => (1.0,  0.75),
            Monster::Zombie  => (1.0,  1.50),
        }
    }
}


// end data/race.rs
