// data/battle.rs

use data::entity::Entity;

/// A Battle is a simulation between two Entities, and can
/// be simulated until one is defeated. The Battle can
/// be between a player and a monster, or even
/// two players or two monsters.
pub struct Battle {
    pub left:   Entity,
    pub right:  Entity,
}


impl Battle {
    /// Instantiate a new battle between two entities. If the two entities
    /// are not different (Player vs Player or Monster vs Monster),
    /// First Strike will always be assigned to the SECOND entity passed in
    /// if they are equal level with no First Strike passives.
    /// (Two Level 10 Rogues fighting eachother, the second one passed in here
    /// will always go first. This is technically undefined behavior by Desktop
    /// Dungeon rules, so this will be how it is defined here).
    pub fn new(p: Entity, m: Entity) -> Battle {
        Battle {
            left: p,
            right: m,
        }
    }


    /// Run the simulation and print the results to stdout.
    ///
    /// ```
    /// let player = Entity::new_player(1, Race::Human, Class::Priest)?;
    /// let monster = Entity::new_monster(1, Monster::Meatman)?;
    /// Battle::new(player, monster).run();
    /// ```
    pub fn run(&mut self) -> Result<i32, String> {
        let mut turns : usize = 1;
        let mut left_first : bool = false;

        // if player is higher level, player attacks first
        if self.left.level() > self.right.level() {
            left_first = true;
        }

        // if player has first strike and other monster doesn't, player first
        if self.left.first_strike() && !self.right.first_strike() {
            left_first = true;
        }

        println!();
        println!("{} is fighting a {}!",
                 self.left.full_name(),
                 self.right.full_name()
        );
        println!();
        self.left.status();
        self.right.status();
        
        let mut running : bool = true;
        while (self.left.hp() != 0) && (self.right.hp() != 0) && running
        {
            println!("--------------- Turn {} ---------------", turns);

            // if player goes first, evaluate the player swing, then monster's
            // TODO: reduce boilerplate somehow? probably not possible
            if left_first {
                self.left.swing_at(&mut self.right);

                if self.right.hp() == 0 {
                    println!("{} has died!", self.right.to_string());
                    running = false;
                } else {
                    self.right.swing_at(&mut self.left);

                    if self.left.hp() == 0 {
                        println!("{} has died!", self.left.to_string());
                        running = false;
                    }
                }
            } else {
                self.right.swing_at(&mut self.left);

                if self.left.hp() == 0 {
                    println!("{} has died!", self.left.to_string());
                    running = false;
                } else {
                    self.left.swing_at(&mut self.right);

                    if self.right.hp() == 0 {
                        println!("{} has died!", self.right.to_string());
                        running = false;
                    }
                }
            }

            // print out values if the battle is still going
            if running {
                self.left.status();
                self.right.status();
            }

            // bump the turns counter
            println!();
            turns += 1;
        }

        println!("--------------- Results ---------------");
        println!("{} has won!", match self.left.hp() {
            0 => self.right.to_string(),
            _ => self.left.to_string(),
        }); 
        println!();
        
        return Ok(0);
    }
}


// end data/battle.rs

