// src/bin/meatman_vs_zombie.rs

// The ultimate battle between Desktop Dungeon Gods

extern crate dd_sim;
use dd_sim::prelude::*;

fn meatman_vs_zombie() -> Result<i32, String> {
    let meatman = Entity::new_monster(10, Monster::Meatman)?;
    let zombie  = Entity::new_monster(10, Monster::Zombie)?;

    return Battle::new(meatman, zombie).run();
}

fn main() {
    match meatman_vs_zombie() {
        Err(e) => println!("{}", e),
        _ => {},
    }
}

// end src/bin/meatman_vs_zombie.rs
