// src/bin/priest_vs_undead.rs

extern crate dd_sim;
use dd_sim::prelude::*;

/// Run a priest vs. a zombie monster.
fn priest_vs_zombie() -> Result<i32, String> {
    let priest = Entity::new_player(10, Race::Human, Class::Priest)?;
    let zombie = Entity::new_monster(9, Monster::Zombie)?;

    return Battle::new(priest, zombie).run();
}


/// Run a priest vs. a vampire monster.
fn priest_vs_vampire() -> Result<i32, String> {
    let priest = Entity::new_player(10, Race::Human, Class::Priest)?;
    let vampire = Entity::new_monster(9, Monster::Zombie)?;

    return Battle::new(priest, vampire).run();
}


/// Run both priest_vs_zombie() and priest_vs_vampire()
fn main() {
    match priest_vs_zombie() {
        Err(e) => println!("Err: {}", e),
        _ => {},
    }

    match priest_vs_vampire() {
        Err(e) => println!("Err: {}", e),
        _ => {},
    }
}

// end src/bin/priest_vs_undead.rs
