// src/prelude.rs


/// Imports common structs/enums/traits to be used for main programs
/// common use:
/// ```
/// extern crate dd_sim;
/// use dd_sim::prelude::*;
/// ```
pub use data::race::{Race, Monster};
pub use data::class::Class;
pub use data::entity::Entity;
pub use data::battle::Battle;


// end src/prelude.rs
